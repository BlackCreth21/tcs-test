package pages;

import driver.DriverContext;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utils.Utilitarios;

import java.util.ArrayList;
import java.util.List;

public class SymbolEarningData {
    @FindBy(css = "th.earnings-surprise__columnheader")
    private List<WebElement> tableTitulo;

    @FindBy(css = "td.earnings-surprise__table-cell")
    private List<WebElement> tableDatos;

    private ArrayList<String> lista;


    public SymbolEarningData(){
        PageFactory.initElements(DriverContext.getDriver(), this);
    }

    public ArrayList<String> getTextTableTitulo() {
        lista = new ArrayList<String>();
        for(int i=0; i < tableTitulo.size(); i++){
            lista.add(tableTitulo.get(i).getText());
        }

        return lista;
    }

    public ArrayList<String> getTextTableDatos(){
        lista = new ArrayList<String>();
        for(int i=1; i < tableDatos.size(); i++){
            lista.add(tableDatos.get(i).getText());
            i=i+3;
        }

        return lista;
    }

    public void validarObjetos() {
        for (int i = 0; i < tableTitulo.size(); i++) {
            Utilitarios.validarObjeto(tableTitulo.get(i));
            Utilitarios.highlight(tableTitulo.get(i));
        }

        for (int i = 0; i < tableDatos.size(); i++) {
            Utilitarios.validarObjeto(tableDatos.get(i));
            Utilitarios.highlight(tableDatos.get(i));

        }
    }
}