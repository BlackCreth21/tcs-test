package pages;

import driver.DriverContext;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utils.Utilitarios;

import java.util.ArrayList;
import java.util.List;

public class EarningsCalendar {
    @FindBy(css = "h1.market-calendar__title")
    private WebElement lblTitulo;

    @FindBy(css = "div.market-calendar-table__cell-content > a")
    private List<WebElement> lnkSymbol;

    private ArrayList<String> lista;

    public EarningsCalendar(){
        PageFactory.initElements(DriverContext.getDriver(), this);
    }

    public void validarObjetos(){
        Utilitarios.esperarObjeto(lblTitulo);

        Utilitarios.validarObjeto(lblTitulo);
        Utilitarios.highlight(lblTitulo);
        for(int i=0; i < lnkSymbol.size(); i++){
            Utilitarios.validarObjeto(lnkSymbol.get(i));
            Utilitarios.highlight(lnkSymbol.get(i));
        }
    }

    public ArrayList<String> getTextLinkSymbol(){
        lista = new ArrayList<String>();
        for(int i=0; i < lnkSymbol.size(); i++){
            lista.add(lnkSymbol.get(i).getText());
        }
        return lista;
    }

    public int obtenerTotalFilas(){
        return lnkSymbol.size();
    }

    public void ingresarAlSymbol(int posicion){
        lnkSymbol.get(posicion).sendKeys(Keys.ENTER);
    }
}