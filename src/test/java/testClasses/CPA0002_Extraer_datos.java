package testClasses;

import driver.DriverContext;
import pages.EarningsCalendar;
import pages.SymbolEarningData;
import utils.FormatoExcel;

import java.util.ArrayList;

public class CPA0002_Extraer_datos {
    private int totalFilas;
    private ArrayList<String> listaSymbol;

    private FormatoExcel excel;
    private EarningsCalendar earningsCalendar;
    private SymbolEarningData symbolEarningData;

    public void extraccionDatos(){

    }

    public void CPA0002_Extraer_datos_de_tabla_earning_data(){
        earningsCalendar = new EarningsCalendar();
        listaSymbol = earningsCalendar.getTextLinkSymbol();
        excel = new FormatoExcel();
        totalFilas = earningsCalendar.obtenerTotalFilas();

        for(int i=0; i < 2; i++) {
            earningsCalendar.ingresarAlSymbol(i);
            symbolEarningData = new SymbolEarningData();

            excel.setDatosRecopilados(symbolEarningData.getTextTableTitulo(),
                    symbolEarningData.getTextTableDatos());
            excel.crearHoja(listaSymbol.get(i));
            excel.insertarDatos();
            DriverContext.getDriver().navigate().back();
            earningsCalendar = new EarningsCalendar();
            listaSymbol = earningsCalendar.getTextLinkSymbol();
        }
        excel.cerrarDocumento();
    }
}
