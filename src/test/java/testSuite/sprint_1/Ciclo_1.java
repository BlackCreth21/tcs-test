package testSuite.sprint_1;

import constantes.Navegador;
import driver.DriverContext;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import testClasses.CPA0002_Extraer_datos;
import utils.ReadProperties;

public class Ciclo_1 {
    private CPA0002_Extraer_datos extraerDatos;

    @BeforeMethod
    public void start(){
        DriverContext.setUp(Navegador.Chrome, ReadProperties.readFromConfig("urlSite.properties").getProperty("url"));
    }

    @AfterMethod
    public void close(){
        DriverContext.quitDriver();
    }

    @Test
    public void test(){
        extraerDatos = new CPA0002_Extraer_datos();
        extraerDatos.CPA0002_Extraer_datos_de_tabla_earning_data();
    }
}
