package testSuite.sprint_2;

import constantes.Navegador;
import driver.DriverContext;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import testClasses.CPA0001_Validar_objetos;
import utils.ReadProperties;

public class Ciclo_1 {
    public CPA0001_Validar_objetos validarObjetos;

    @BeforeMethod
    public void start(){
        DriverContext.setUp(Navegador.Chrome, ReadProperties.readFromConfig("urlSite.properties").getProperty("url"));
    }

    @AfterMethod
    public void close(){
        DriverContext.quitDriver();
    }

    @Test
    public void test(){
        validarObjetos = new CPA0001_Validar_objetos();
        validarObjetos.CPA0001_validar_objetos_Earning_Calendar();
    }
}
