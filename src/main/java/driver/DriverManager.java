package driver;

import constantes.Navegador;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

public class DriverManager {
    private DesiredCapabilities capabilities = new DesiredCapabilities();
    private WebDriver webDriver;
    private String root = "src\\webDrivers\\";

    protected void resolveDriver(Navegador nav,String ambURL) {
        switch (nav) {
            case Chrome:
                System.setProperty("webdriver.chrome.driver", root + "chromedriver.exe");
                webDriver = new ChromeDriver();
                capabilities.setBrowserName("Chrome");
                break;
            case Firefox:
                System.setProperty("webdriver.gecko.driver", root + "geckodriver.exe");
                webDriver = new FirefoxDriver();
                capabilities.setBrowserName("Firefox");
                break;
            case Edge:
                System.setProperty("webdriver.edge.driver", root + "msedgedriver.exe");
                webDriver = new EdgeDriver();
                capabilities.setBrowserName("Microsoft Edge");
                break;
            default:
                break;
        }

        webDriver.manage().deleteAllCookies();
        webDriver.manage().window().maximize();
        webDriver.get(ambURL);
    }

    protected WebDriver getDriver() { return webDriver; }
}
