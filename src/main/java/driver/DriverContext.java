package driver;

import constantes.Navegador;
import org.openqa.selenium.WebDriver;

public class DriverContext {
    private static DriverManager driverManager = new DriverManager();

    public static void setUp(Navegador nav, String ambURL) {
        driverManager.resolveDriver(nav,ambURL);
    }
    public static WebDriver getDriver()
    {
        return driverManager.getDriver();
    }

    public static void quitDriver() {
        driverManager.getDriver().quit();
    }
}