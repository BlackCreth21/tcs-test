package utils;

import driver.DriverContext;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.testng.asserts.SoftAssert;

import java.time.LocalDateTime;

public class Utilitarios {
    public static boolean validarObjeto(WebElement objeto){
        return objeto.isDisplayed();
    }

    public static void pausa(long segundo){
        try{
            Thread.sleep(segundo*1000);
        }catch(InterruptedException iexc){
            System.out.println("Error en el tiempo de espera: [" + iexc.getMessage() + "]");
        }
    }

    public static void highlight(WebElement objeto) {
        JavascriptExecutor jExecutor;
        try {
            jExecutor = (JavascriptExecutor) DriverContext.getDriver();

            for (int i = 0; i < 2; i++) {
                jExecutor.executeScript("arguments[0].setAttribute('style', arguments[1]);", objeto, "color: black; border: 5px solid black;");
                pausa(1);
                jExecutor.executeScript("arguments[0].setAttribute('style', arguments[1]);", objeto, "");
            }
        }catch(NoSuchElementException noSuch){
            System.out.println("No se reconoce el elemento...");
        }
    }

    public static boolean isNumerico(String cadena) {
        boolean resultado;

        try {
            Integer.parseInt(cadena);
            resultado = true;
        } catch (NumberFormatException excepcion) { resultado = false; }

        return resultado;
    }

    public static boolean validarTexto(String textoApp,String textoAValidar){
        if (textoApp.equals(textoAValidar)){
            return true;
        }else {
            return false;
        }
    }

    public static void esperarObjeto(WebElement objetoEsperado){
        boolean visible = false;
        LocalDateTime fechaInicio = LocalDateTime.now();

        while(!visible){
            LocalDateTime fechaLoop = LocalDateTime.now();
            try{
                if(objetoEsperado.isDisplayed()){
                    visible = true;
                    System.out.println("##Objeto encontrado....##");
                }
            }catch(Exception exception) {
                if (fechaLoop.compareTo(fechaInicio) % 5 == 0) {
                    System.out.println("##Esperando objeto....##");
                }
            }
        }
    }

    public static void cerrarValidacion(SoftAssert assertReporte){
        try{
            assertReporte.assertAll();
        }catch(Throwable trow){
            System.out.println(trow.getMessage());
        }
    }
}
