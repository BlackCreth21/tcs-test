package utils;

import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Objects;

public class FormatoExcel {
    public String nombreArchivo;
    public String pathArchivo;
    public ArrayList<String> cabecera;
    public ArrayList<String> datos;
    public Calendar calendario;
    public WorkbookSettings wkb_setings;
    public WritableWorkbook wkb;
    public WritableSheet wts;

    public FormatoExcel(){


        calendario = new GregorianCalendar();
        this.nombreArchivo = "Datos_Recopilados-"+calendario.get(Calendar.YEAR)+
                calendario.get(Calendar.MONTH)+calendario.get(Calendar.DAY_OF_MONTH)+calendario.get(Calendar.HOUR)+
                calendario.get(Calendar.MINUTE)+calendario.get(Calendar.SECOND)+".xls";
        this.pathArchivo = "src\\excels\\";

        this.wkb_setings = new jxl.WorkbookSettings();
        this.wkb_setings.setEncoding("ISO-8859-1");
        try {
            this.wkb = Workbook.createWorkbook(new File(pathArchivo+nombreArchivo), wkb_setings);
        } catch (IOException e) {
            System.out.println("No se puede generar el excel");
        }
    }

    public void setDatosRecopilados(ArrayList<String> cabecera, ArrayList<String> datos){
        this.cabecera = cabecera;
        this.datos = datos;
    }

    public void crearHoja(String nombreHoja) {
        try {
            wkb.setOutputFile(new File(pathArchivo+nombreArchivo));
            wts = wkb.createSheet(nombreHoja, 0);
        } catch (NullPointerException npe) {
            System.out.println("No existe la hoja");
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            wts = wkb.getSheet(0);
        }catch(NullPointerException npe){
            System.out.println("Archivo no encontrado");
        }
    }

    public void insertarDatos(){
        String textoObtenido;

        try {
            textoObtenido = cabecera.get(2);
            wts.insertRow(1);
            wts.addCell(new Label(0,0,textoObtenido));


            for(int i=0; i < datos.size(); i++){
                textoObtenido = datos.get(i);
                wts.insertRow(1);
                wts.addCell(new Label(0,i+1,textoObtenido));
            }
        }catch (RowsExceededException ree)
            {System.out.println("Cantidad de filas excede el maximo");}
        catch (WriteException we)
            {System.out.println("No se pudo editar la hoja");}
        catch (NullPointerException npe)
            {System.out.println("No se pudo continuar con la edicion del documento");}
    }

    public void cerrarDocumento(){
        try {
            this.wkb.write();
            this.wkb.close();
        }catch (WriteException we)
        {System.out.println("No se pudo modificar el excel"); }
        catch (IOException ioe)
        {System.out.println("No se pudo encontrar el excel"); }
        catch (NullPointerException npe)
        {System.out.println("No se pudo agregar la hoja"); }
    }
}